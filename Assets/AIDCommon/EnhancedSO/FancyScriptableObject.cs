﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

//support singleton data
//support saving changes to disk
//support loading local newest from disk
//support resetting local, basical clear local disk changes

public abstract class FancyScriptableObject<T> : ScriptableObject where T : ScriptableObject
{
    [InspectorButton("SaveAsJSON", ButtonWidth = 160)]
    public bool saveAsJSON;
    [InspectorButton("LoadFromJSON", ButtonWidth = 160)]
    public bool loadAsJSON;
    [InspectorButton("LoadFromExternalJSON", ButtonWidth = 160)]
    public bool loadFromExternalJSON;

    public string serialisedObjectName = "";

    public string GetStorageLocation()
    {
        return "./" + this.GetType().ToString() + "/" + serialisedObjectName;
    }

    public void SaveAsJSON()
    {
        var loc = GetStorageLocation() + ".json";
        
        Directory.CreateDirectory(Path.GetDirectoryName(loc));
        File.WriteAllText(loc, JsonUtility.ToJson(this,
#if UNITY_EDITOR
            true
#else
            false
#endif
            ));

		Debug.Log("Saved " + serialisedObjectName + " to " + loc);
    }

    public void LoadFromJSON()
    {
        var data = "";

        try
        {
            data = File.ReadAllText(GetStorageLocation() + ".json");
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }

        if(!string.IsNullOrEmpty(data))
            JsonUtility.FromJsonOverwrite(data, this);
    }

    public void LoadFromExternalJSON()
    {
#if UNITY_EDITOR
        var data = "";

        string path = UnityEditor.EditorUtility.OpenFilePanel("Overwrite with external json", "", "json");

        try
        {
            data = File.ReadAllText(path);
        }
        catch (Exception)
        {

        }

        if (!string.IsNullOrEmpty(data))
            JsonUtility.FromJsonOverwrite(data, this);
#endif
    }
    
    public static T[] GetInstances()
    {
        var res = Resources.LoadAll<T>("");
        return res;
    }

    public static T GetFirstInstance()
    {
        var res = GetInstances();
        if (res != null && res.Length > 0)
            return res[0];
        return null;
    }

}
