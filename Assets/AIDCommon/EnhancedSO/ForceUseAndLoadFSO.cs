﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceUseAndLoadFSO : MonoBehaviour
{
    public UnityEngine.UI.Text anInt, aFloat, aPrefabName;
    public TestFancyScriptableObject testFSO;
    // Use this for initialization
    void Start()
    {
        ShowCurrentState();
    }


    public void OnClick()
    {
        StartCoroutine(RunTest());
    }

    IEnumerator RunTest()
    {
        yield return null;

        //show current state of two variables

        ShowCurrentState();

        yield return new WaitForSeconds(1);

        //load 

        testFSO.LoadFromJSON();

        ShowCurrentState();
    }



    void ShowCurrentState()
    {
        anInt.text = testFSO.i.ToString();
        aFloat.text = testFSO.dmg.ToString();
        aPrefabName.text = testFSO.prefab.ToString();
    }
}
