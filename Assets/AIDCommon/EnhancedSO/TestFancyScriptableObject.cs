﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class TestFancyScriptableObject : FancyScriptableObject<TestFancyScriptableObject>
{
    public int i;
	public float dmg;

    //as long you aren't loading the entire thing outside of UnityEditor, eg, at runtime then this is fine,
    // if you intend to load from external it needs to be simple data that the JSONUtility can handle correctly
	public GameObject prefab;

	public AudioClip clip;
	public Sprite spr;
}
